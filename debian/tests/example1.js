
const NumberAllocator = require('number-allocator').NumberAllocator
const assert = require('assert')

// Construct a NumerAllocator that has [0-10] numbers.
// All numbers are vacant.
const a = new NumberAllocator(0, 10)

// Allocate the least vacant number.
const num0 = a.alloc()
console.log(num0) // 0
assert.equal(num0, 0)


// Allocate the least vacant number.
const num1 = a.alloc()
console.log(num1) // 1
assert.equal(num1, 1)

// Use any vacant number.
const ret1 = a.use(5) // 5 is marked as used(occupied) in the NumberAllocator.
console.log(ret1) // true
assert(ret1)

// If use the used number, then return false.
const ret2 = a.use(1) // 1 has already been used, then return false
console.log(ret2) // false
assert(!ret2)

// Free the used number.
a.free(1)

// Clear all used mark. Now [0-10] are allocatable again.
a.clear()
